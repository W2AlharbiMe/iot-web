/**
 * this is IoT routes
 * 
 * GET /api/settings
 * 
 * this route is used to get the user settings
 * 
 * ----------------------------
 * 
 * 
 * POST /api/current
 * 
 * this route is used to save the current sensor readings in memory
 * 
 * 
 */

const { Router } = require('express');


module.exports = (store) => {
    const r = Router();
    const state = store.get();

    r.get('/settings', (req, res) => {
        const { temp, humidity } = state.settings;
        const r = `${temp},${humidity}`;

        store.setLastUserSync(new Date());

        return res.send(r);
    });


    r.post('/current', (req, res) => {
        const { temp, hum: humidity, status } = req.body;

        store.setCurrent({
            temp,
            humidity,
            status
        }).setLastCurrentSync(new Date());

        return res.send('ok');
    });
    

    return r;
};
