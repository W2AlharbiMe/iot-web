/**
 * this is the frontend routes
 * 
 * GET /api/sensor
 * 
 * returns the sensor readings
 * 
 * 
 * -------------------------------
 * 
 * 
 * POST /api/sensor
 * 
 * set user settings
 * 
 * -------------------------------
 * 
 * 
 * GET /api/last
 * 
 * returns the last time the server and the device synced configurations
 * 
 */

const { Router } = require("express");
const cors = require('cors');


module.exports = (store) => {
    const r = Router();

    const state = store.get();

    r.use(cors());

    r.get('/sensor', (req, res) => {
        return res.json(state);
    });

    
    r.post('/sensor', (req, res) => {
        const { temp, hum: humidity } = req.body;
        
        store.setUserSettings({
            temp,
            humidity
        });


        return res.json(store.get());
    });


    r.get('/last', (req, res) => {
        const sync = store.getSyncTime();

        return res.json(sync);
    });

    return r;
};
