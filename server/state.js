const { formatDistance } = require("date-fns");


const store = () => {
    const _default = {
        current: {
            temp: 0,
            humidity: 0,
            status: 0,
        },
    
        settings: {
            temp: 27,
            humidity: 30
        },


        // when was the last time the API synced with the device
        _lastCurrentSync: 0,

        // when was the last time the device synced with user settings
        _lastUserSync: 0,
    };

    return {
        // in memory database API

        get() {
            return _default;
        },

        setCurrent(current) {
            _default.current = current;
            return this;
        },

        setUserSettings(settings) {
            _default.settings = settings;
            return this;
        },

        setLastCurrentSync(time) {
            _default._lastCurrentSync = time;
            return this;
        },

        setLastUserSync(time) {
            _default._lastUserSync = time;
            return this;
        },

        getSyncTime() {
            const response = {
                valid: false,
            };


            if(_default._lastCurrentSync !== 0) {
                response.valid = true;
                response.current = formatDistance(_default._lastCurrentSync, new Date(), { addSuffix: true });
            }

            if(_default._lastUserSync !== 0) {
                response.valid = true;
                response.settings = formatDistance(_default._lastUserSync, new Date(), { addSuffix: true });
            }

            return response;
        }
    }
};



module.exports = store;
