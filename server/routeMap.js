const _ = '/api';

module.exports = {
    IoT: {
        GET: [
            ['use this to read the user settings', '/api/settings']
        ],

        POST: [
            ['use this to send the current sensor readings', '/api/current']
        ],
    },


    frontend: {
        GET: [
            ['use this to get device state', '/api/sensor'],
            ['use this to know at which time the server and the device synced configurations', '/api/last']
        ],
        POST: [
            ['use this to set user settings', '/api/sensor']
        ],
    },
};
