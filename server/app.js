const { json, urlencoded } = require('express');
const express = require('express');
const routeMap = require('./routeMap');
const frontend = require('./routes/frontend');
const iot = require('./routes/iot');
const state = require('./state');


const app = express();
const store = state();
const PORT = 8001;



app.disable('x-powered-by');
app.disable('etag');


app.use(
    json(),

    urlencoded({ extended: false, }),
);


app.get('/', (req, res) => {
    return res.json(routeMap);
});


app.use(
    '/api',

    iot(store),

    frontend(store),
);


app.listen(PORT, () => console.log(`server is running at http://localhost:${PORT}`));
