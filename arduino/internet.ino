#include <SPI.h>
#include <Ethernet.h>
#include <ArduinoHttpClient.h>

// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};

IPAddress ip = IPAddress(192, 168, 2, 2);
IPAddress gateway = IPAddress(192, 168, 2, 1);
IPAddress dns = IPAddress(192, 168, 2, 1);
IPAddress subnet = IPAddress(255, 255, 255, 0);

// Name of the server we want to connect to
char serverAddress[] = "192.168.2.1";  // server address
int port = 3000;

// Path to download (this is the bit after the hostname in the URL
// that you want to download
const char targetPath[] = "/target";
const char updatePath[] = "/current";

EthernetClient ether;
HttpClient http(ether, serverAddress, port);

void setupEthernet() {
  Serial.println("Initialize Ethernet:");

  Ethernet.begin(mac, ip, dns, gateway, subnet);

  // print your local IP address:
  Serial.print("My IP address: ");
  Serial.println(Ethernet.localIP());
}

String getResponse(int err) {

  if (err == 0) {
    Serial.println("startedRequest ok");

    err = http.responseStatusCode();
    if (err == 200) {
      Serial.print("Got status code: ");
      Serial.println(err);

      String response = http.responseBody();
      Serial.print("Body returned follows: ");
      Serial.println(response);
      return response;

    } else {
      Serial.print("Getting response failed: ");
      Serial.println(err);
      return "N/A";
    }
  } else {
    Serial.print("\nConnect failed: ");
    Serial.println(err);
    return "N/A";
  }
}

void getData() {
  int res = http.get(targetPath);
  String result = getResponse(res);

  if (result != "N/A") {
    targetTemp = result.substring(0, result.indexOf(',')).toFloat();
    targetHumd = result.substring(result.indexOf(',') + 1).toFloat();
  }

  http.stop();
}

void sendData() {
  String contentType = "application/x-www-form-urlencoded";
  String postData = "temp=" + String(temperature);
  postData += "&humd=" + String(humidity);
  postData += "&status=" + String(status);

  int res = http.post(updatePath, contentType, postData);
  if (getResponse(res) != "N/A") {
    Serial.print("Data sent successfully.");
  }

  http.stop();
}
