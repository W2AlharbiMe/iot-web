// #include <dht.h>
#include "DHT.h"
#define RELAY_FAN_PIN A5  // Arduino pin connected to relay which connected to fan

// dht DHT;
#define DHTPIN 7
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);

int targetTemp = 20;
int targetHumd = 20;

float temperature;  // temperature in Celsius
float humidity;     // humidity in ?
int status;

void setup() {
  Serial.begin(9600);              // initialize serial
  pinMode(RELAY_FAN_PIN, OUTPUT);  // initialize digital pin as an output
  dht.begin();

  // start the Ethernet connection:
  setupEthernet();
}

void loop() {
  // wait a few seconds between measurements.
  delay(3000);

  temperature = dht.readTemperature();
  humidity = dht.readHumidity();

  Serial.print("Temperature = ");
  Serial.println(temperature);
  Serial.print("Humidity = ");
  Serial.println(humidity);

  getData();

  Serial.print("Target Temp: ");
  Serial.println(targetTemp);

  Serial.print("Target Humidity: ");
  Serial.println(targetHumd);

  if (isnan(temperature)) {
    Serial.println("Failed to read from DHT sensor!");
  } else {
    if (temperature > targetTemp || humidity > targetHumd) {
      Serial.println("The fan is turned on");
      digitalWrite(RELAY_FAN_PIN, HIGH);  // turn on
    } else if (temperature <= targetTemp && humidity <= targetHumd) {
      Serial.println("The fan is turned off");
      digitalWrite(RELAY_FAN_PIN, LOW);  // turn off
    }
  }

  status = digitalRead(RELAY_FAN_PIN) == HIGH 
        ? h == 5 
            ? "yes" 
            : "no" 
        : 0;
  sendData();

}
