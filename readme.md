### Automatic Cooling System
---

presentation:
https://docs.google.com/presentation/d/1wMJX4wKHiOZC5chX4pYEci6R8gu-lozWswO5xqZ81Nk/edit#slide=id.p


### Outline
- Introduction
- Goal
- Used Technologies
- Routes
- The State Object
- Frontend
- Commands
- Students


### Introduction
A system that control a fan based on room temperature and humidity, the user can control at which temperature and humidity the fan will start.


### Goal
this project was created to graduate for Internet Of Things (IoT) course at CTI.



### Used Technologies
- Runtime Environment: **Node.js**
- Frontend: **Vue.js**
- Backend: **Express.js**
- **Arduino Scripting Language**



### Routes

```md
// IoT

GET /api/settings
- this route is used to get the user settings for the IoT in a string format

POST /api/current
- this route is used to save the current sensor readings in memory

// Frontend

GET /api/sensor
- returns the sensor readings


POST /api/sensor
- save user settings in memory

GET /api/last
- returns the last time the server and the device synced configurations
```


### The State Object:

```js
{
    // sensor readings
    current: {
        temp: 0,
        humidity: 0,
        status: 0,
    },

    // user settings
    settings: {
        temp: 27,
        humidity: 30
    },


    // when was the last time the API synced with the device
    _lastCurrentSync: 0,

    // when was the last time the device synced with user settings
    _lastUserSync: 0,
}
```

### Frontend

**Main Page** <br />
![image](/uploads/148da8aa1797018efd6c679cd5b705ee/image.png)

**Change User Settings** <br />
![image](/uploads/b2e7dbc9cfe2aa550b95644c9722cd36/image.png)


---


### Commands
- `npm run dev` - start server in watch mode on port 8001
- `npm start` - start server in production mode on port 8001



### Students
- Abdullah Alharbi
- Abdullah Aldawsari
