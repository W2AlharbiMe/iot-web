// this is used to send the user settings to the server

import http from "./http";

export default async (payload) => {
    payload.hum = payload.humidity;
    console.log(payload);

    const { data } = await http.post('/sensor', payload);

    return data;
};
