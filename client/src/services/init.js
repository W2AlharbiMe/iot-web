// this is used to GET the server state

import http from "./http";

 
export default async () => {
    const { data } = await http.get('/sensor');

    return data;
};


